#!/bin/bash


for i in {5..28}
do
	if (( $i / 10 < 1 ))
	then
		wget -O ${i}.gif https://spaceweather.com/images2015/0${i}apr15/hmi1898.gif
	else
		wget -O ${i}.gif https://spaceweather.com/images2015/${i}apr15/hmi1898.gif
	fi
done