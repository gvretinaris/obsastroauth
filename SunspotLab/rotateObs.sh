#!/bin/bash

for (( i = 5; i <= 28; ++i )); 
do
    if ((${i} == 5)) || ((${i} == 10)) || ((${i} == 11)) || ((${i} == 12));
    then
        mogrify -rotate 26.2 ${i}.jpg
    elif ((${i} == 6)) || ((${i} == 7)) || ((${i} == 8)) || ((${i} == 9));
    then
        mogrify -rotate 26.3 ${i}.jpg
    elif ((${i} == 13)) || ((${i} == 14)); 
    then
        mogrify -rotate 26.1 ${i}.jpg
    elif ((${i} == 15)) || ((${i} == 16)); 
    then
        mogrify -rotate 26.0 ${i}.jpg
    elif ((${i} == 17)); 
    then
        mogrify -rotate 25.9 ${i}.jpg
    elif ((${i} == 18)) || ((${i} == 19)); 
    then
        mogrify -rotate 25.8 ${i}.jpg
    elif ((${i} == 20)); 
    then
        mogrify -rotate 25.7 ${i}.jpg
    elif ((${i} == 21)); 
    then
        mogrify -rotate 25.6 ${i}.jpg
    elif ((${i} == 22)); 
    then
        mogrify -rotate 25.5 ${i}.jpg
    elif ((${i} == 23)); 
    then
        mogrify -rotate 25.4 ${i}.jpg
    elif ((${i} == 24)); 
    then
        mogrify -rotate 25.2 ${i}.jpg
    elif ((${i} == 25)); 
    then
        mogrify -rotate 25.1 ${i}.jpg
    elif ((${i} == 26)); 
    then
        mogrify -rotate 25.0 ${i}.jpg
    elif ((${i} == 27)); 
    then
        mogrify -rotate 24.8 ${i}.jpg
    elif ((${i} == 28)); 
    then
        mogrify -rotate 24.7 ${i}.jpg

    
    fi
done
