#!/usr/bin/env python

import numpy as np
import csv
import math
import matplotlib.pyplot as plt
import tikzplotlib

g = 0
f = 0
comp = int(5)
WolfNumber = np.zeros(20)
helLat = np.zeros((20,10))
split = '/'
line = 0
time = np.zeros((5,20))
helLong = np.zeros((5,20))
m = 0
groups = [2320,2321,2324,2325,2327]
with open('measurements.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    i = 0
    j = 0
    for row in csv_reader:
        if line == 0:
            line = 1
            continue
        else:
            if comp == int(row[0].partition(split)[0]):
                #Wolf
                g = g + 1
                f = f + int(row[3])
                #Butterfly
                helLat[i][j] = float(row[6])
                j += 1                


            elif comp < int(row[0].partition(split)[0]):
                #Wolf
                WolfNumber[i] = (10*g+f)
                comp = int(row[0].partition(split)[0])
                g = 1
                f = int(row[3])
                i = i + 1
                #Butterfly
                j = 0
                helLat[i][j+1] = float(row[6])

        if int(row[0].partition(split)[0]) == 28 and g == 3:
               WolfNumber[i] = (10*g+f)


        
        for m in range(0,5): 
            if int(row[1]) == groups[m]:

                helLong[m][i] = int(row[5])
                time[m][i] = float(row[7])
 
        line = line + 1

average = np.zeros(5)
iterations = 0
for m in range(0,5):
    for n in range(1,20):
        if abs(helLong[m][n]) < 1 or abs(helLong[m][n-1]) < 1:
            continue
        else:
            average[m] += (helLong[m][n]-helLong[m][n-1])/(time[m][n]-time[m][n-1])
            iterations += 1

    average[m] = average[m]/iterations*24
    iterations = 0

averageVal = np.average(average)

print('The average angular velocity of all suspots is',averageVal)
print()
print(average)


for i in range(0,20):
    for j in range(0,10):
        if helLat[i][j] == 0:
            helLat[i][j] = np.nan



k=5
days = np.zeros(20)
for j in range(0,20):
    if j == 10-5 or k == 13 or k == 17 or k == 26:
        k = k + 1

    print("Wolf Number for ", k, "April 2015 is", WolfNumber[j])
    days[j] = k
    k = k + 1


fig = plt.figure(figsize=(8, 8))
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

ax1.set_xlabel("Day of April")
ax1.set_ylabel("R")

ax1.plot(days,WolfNumber,'--')
ax1.plot(days,WolfNumber,'ro')


ax2.set_ylabel("Day of April")
ax2.set_xlabel("Heliographic Latitude")
ax2.set(xlim=(-22, 22))
ax2.plot(helLat,days,'k.')
ax2.axvline(0,color='black',linewidth = '0.75')
plt.show()


#Αυτή η εντολή είναι για να έχω τα γραφήματα σε κώδικα latex ώστε
#να μην χρειάζομαι να καλώ το γράφημα σαν αρχείο.
#tikzplotlib.save("Butterfly.tex")