#!/usr/bin/python3

import numpy as np
import csv
import math
import matplotlib
import matplotlib.pyplot as plt  
import os
import tikzplotlib


firstline = 0
r0 = 0.2

appMagn = np.zeros(25)
absMagn = np.zeros(25)
calibrConst = np.zeros(25)
obsVel =  np.zeros(25)
realVel =  np.zeros(25)
distance = np.zeros(25)
incAngle = np.zeros(25)
ba = np.zeros(25)

with open('T-F_data.txt', 'r') as txt_file:
    i = 0
    for row in txt_file:
        if firstline < 2:
            firstline += 1
            continue
        else:
            columns = row.split()
            appMagn[i] = float(columns[0])
            ba[i] = float(columns[1])
            obsVel[i] = float(columns[2])
            i += 1

meanX = 0
meanR = 0
for i in range(0,25):
    incAngle[i] = np.arccos( (ba[i]*ba[i] - r0*r0)/(1-r0*r0) )
    realVel[i] = obsVel[i]/np.sin(incAngle[i])
    absMagn[i] = -8.09*(np.log10(realVel[i])-2.5)-21.05
    calibrConst[i] = appMagn[i]+8.09*(np.log10(realVel[i])-2.5)
    exponential = (appMagn[i]-absMagn[i]-25)/5
    distance[i] = np.power(10,exponential)
    meanX += calibrConst[i]
    meanR += distance[i]

meanX = meanX/25
meanR = meanR/25

print(meanR,meanX)

# np.savetxt("resultsTF.csv", np.c_[absMagn,realVel,calibrConst,distance],
#            delimiter=",",fmt='%1.3f')

# command = "sed -i '1 i\M_R,W_real,X,r(Mpc)' resultsTF.csv"
# os.system(command)






# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     "pgf.texsystem": "xelatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': True,
# })




# fig = plt.figure(figsize=(8, 8))
# ax1 = fig.add_subplot(211)
# ax2 = fig.add_subplot(212)

# ax1.plot(np.log10(realVel),absMagn,'ko',mfc='None',label="W_real")
# ax1.plot(np.log10(obsVel),absMagn,'ro',mfc='None',label="W_obs")

# ax1.set_xlabel("log10(W)")
# ax1.set_ylabel("M_R")
# ax1.legend()

# ax2.plot(np.log10(realVel),appMagn,'ko',mfc='None',label="W_real")
# ax2.plot(np.log10(obsVel),appMagn,'ro',mfc='None',label="W_obs")

# ax2.set_xlabel("log10(W)")
# ax2.set_ylabel("m_R")
# ax2.legend()

# #plt.show()
# plt.savefig('tfplot.pgf',dpi=800,bbox_inches='tight')