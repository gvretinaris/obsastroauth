#!/usr/bin/python3

import numpy as np
import csv
import math
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams.update({'errorbar.capsize': 2})
plt.rcParams['hatch.linewidth'] = 2
plt.rcParams['hatch.color'] = 'black'
import tikzplotlib
from PIL import Image
from numpy import asarray
import os


emissionWL = np.zeros(90,dtype='object')
absorbtionWL = np.zeros(90,dtype='object')
Hubble = np.zeros(8)
#redshift = np.zeros(5)
meanRed = np.zeros(8)
deviation = np.zeros(8)
redshift = np.zeros((8,5),dtype='object')

#Pixel to Wavelength ratio
PtWL = (9200-4000)/(832)
#Known absorbtion and emission line wavelengths
ABS = np.array([3933.7,3968.5,4304.4,5175.3,5894])
EM = np.array([6716,6562.8,5006.8,4861.3,3727.3])

for iteration in range(1,9):
    k=0
    l=0
    emissionWL = np.zeros(90,dtype='object')
    absorbtionWL = np.zeros(90,dtype='object')

    image = Image.open(str(iteration)+'.png')
    # summarize some details about the image
    data = asarray(image)

    print(iteration)

    for i in range(114,696):
        for j in range(120,1015):
            if (data[i][j][2] == 253 or data[i][j][2] == 254 or data[i][j][2] == 252) and data[i][j][0] == 0:
                if j in emissionWL or j-1 in emissionWL or j+1 in emissionWL:
                    continue
                else:
                    emissionWL[k] = j
                    k += 1
            elif data[i][j][0] == 253 and data[i][j][2] == 0:
                if j in absorbtionWL or j-1 in absorbtionWL or j+1 in absorbtionWL:
                    continue
                else:
                    absorbtionWL[l] = j
                    l += 1
            else:
                continue

    emissionWL = np.trim_zeros(emissionWL)
    absorbtionWL = np.trim_zeros(absorbtionWL)
    #Pixel to Angstrom transform
    emissionWL = ((emissionWL-167)*PtWL+4010)
    absorbtionWL = ((absorbtionWL-167)*PtWL+4010)
    #Descending sorting
    emissionWL = -np.sort(-emissionWL)
    #Ascending sorting
    absorbtionWL = np.sort(absorbtionWL)

    if iteration < 4:
        for i in range(0,5):
            redshift[iteration-1][i] = abs(absorbtionWL[i] - ABS[i])/ABS[i]
            meanRed[iteration-1] += redshift[iteration-1][i]
        print(redshift[iteration-1])

    elif iteration > 4 and iteration < 9:
        k=0
        for i in range(0,5):
            if i == 0 or i == 1 or k == 8:
                k = k + 1
                redshift[iteration-1][i] = abs(emissionWL[k] - EM[i])/EM[i]
                meanRed[iteration-1] += redshift[iteration-1][i]
            elif k == 4:
                k+=3
                redshift[iteration-1][i] = abs(emissionWL[k+1] - EM[i])/EM[i]
                meanRed[iteration-1] += redshift[iteration-1][i]
            elif i == 4:
                redshift[iteration-1][i] = abs(emissionWL[-1] - EM[-1])/EM[-1]
                meanRed[iteration-1] += redshift[iteration-1][i]
            k+=1
        print(redshift[iteration-1])
        
    elif iteration == 4:
        #Was done by had due to the plot's morphology
        #Which is why this block of code is ugly
        #HeI emission
        #https://physics.nist.gov/PhysRefData/Handbook/Tables/heliumtable2.htm
        redshift[iteration-1][0] = abs(emissionWL[3] - 5843)/5843
        meanRed[iteration-1] += redshift[iteration-1][0]
        #OIII emission
        redshift[iteration-1][1] = abs(emissionWL[4] - EM[2])/EM[2]
        meanRed[iteration-1] += redshift[iteration-1][1]
        #Hγ emission
        redshift[iteration-1][2] = abs(emissionWL[7] - 4340)/4340
        meanRed[iteration-1] += redshift[iteration-1][2]
        print(redshift[iteration-1][:3])


        
meanRed = meanRed/5
meanRed[3]=meanRed[3]*5/3

for i in range(0,8):
    if i == 3:
        for j in range(0,3):
            deviation[i] += (redshift[i][j]-meanRed[i])*(redshift[i][j]-meanRed[i])/3
    else:
        for j in range(0,5):
            deviation[i] += (redshift[i][j]-meanRed[i])*(redshift[i][j]-meanRed[i])/5
    deviation[i] = np.sqrt(deviation[i])

Hubble = meanRed*3*1e+5/(71)

np.savetxt("resultsSpectra.csv", np.c_[meanRed,Hubble,deviation],delimiter=",")


#If you run the code one time and the csv file is created
#you no longer need to run it again, further use of the data
#will be done in the localGroup.py
#This was done mostly due to the time-consuming nature of scanning
#each pixel of each plot to identify the spectral features

os.system('python3 localGroup.py')