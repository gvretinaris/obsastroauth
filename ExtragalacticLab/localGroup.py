#!/usr/bin/python3

import numpy as np
import csv
import math
import matplotlib
import matplotlib.pyplot as plt 
import os

d_0 = 27.1284*np.pi/180
a_0 = 192.8595*np.pi/180
l_0 = 122.9320*np.pi/180
c = 3e+5


def transCoord(RA,DEC):
    #J2000
    RA = RA*np.pi/180
    DEC = DEC*np.pi/180
    l = l_0 - np.arctan( (np.cos(DEC)*np.sin(RA-a_0) )/
        (np.sin(DEC)*np.cos(d_0)-np.sin(d_0)*np.cos(DEC)*np.cos(RA-a_0)) )
    b = np.arcsin( np.sin(DEC)*np.sin(d_0)+np.cos(DEC)*np.cos(d_0)*np.cos(RA-a_0) )
    return np.array([l,b])

def toLocalGroup(v,l,b):
    Vlsr = v + 9*np.cos(l)*np.cos(b)+12*np.sin(l)*np.cos(b)+7*np.sin(b)
    Vgal = Vlsr + 220*np.sin(l)*np.cos(b)
    VLG = Vgal - 62*np.cos(l)*np.cos(b)+40*np.sin(l)*np.cos(b) - 35*np.sin(b)
    return VLG


coordinates=np.zeros((8,2))
galacticCoor = np.zeros((8,2))
LocalGroupVelocity = np.zeros(8)
redshift = np.zeros(8)
corrRed = np.zeros(8)
corrDist = np.zeros(8)
corrError = np.zeros(8)
trial = np.zeros(8)
error = np.zeros(8)
hubbDist = np.zeros(8)
deviation = np.zeros(8)
firstline = 0
i = 0
with open('resultsSpectra.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
        redshift[i] = float(row[0])
        hubbDist[i] = float(row[1])
        deviation[i] = float(row[2])
        i+=1

i = 0
with open('table.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
        if firstline == 0:
            firstline = 1
            continue
        else:            
            coordinates[i] = np.array([float(row[1]),float(row[2])])
            i += 1

VirgoGalCoord = transCoord( (12+26/50+32/3600)*np.pi/12 , (12+43/60+24/3600)*np.pi/180 )
VirgoGalVel = toLocalGroup(220,VirgoGalCoord[0],VirgoGalCoord[1])

for i in range(0,8):
    galacticCoor[i] = transCoord(coordinates[i][0],coordinates[i][1])
for i in range(0,8):
    LocalGroupVelocity[i] = toLocalGroup(redshift[i]*c,galacticCoor[i][0],galacticCoor[i][1])
    corrDist[i] = (LocalGroupVelocity[i]-VirgoGalVel)/71
    corrRed[i] =  corrDist[i]*71/c
    corrError[i] = abs(corrRed[i]-redshift[i])/redshift[i]*100

np.savetxt("resultsRedshift.csv", np.c_[redshift,deviation,corrRed,corrError,hubbDist,corrDist],
            delimiter=",",fmt='%1.5f')

command = "sed -i '1 i\z,sigma_z,z_{corr},sigma_z-z,r_H(Mpc),r_{corr}(Mpc)' resultsRedshift.csv"
os.system(command)
