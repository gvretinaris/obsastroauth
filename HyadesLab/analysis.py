 #!/usr/bin/python3

import numpy as np
import csv
import math
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams.update({'errorbar.capsize': 2})
plt.rcParams['hatch.linewidth'] = 2
plt.rcParams['hatch.color'] = 'black'
import tikzplotlib


#RA = np.zeros(35)
#DEC = np.zeros(35)
names = np.zeros(35)
mu = np.zeros(35)
theta = np.zeros(35)
TangVel = np.zeros(35)
Distance = np.zeros(35)
paralaxDist = np.zeros(35)

errormu = np.zeros(35)
errorTangVel = np.zeros(35)
errorDistance = np.zeros(35)
errorParalaxDist = np.zeros(35)
diffError = np.zeros(35)

hyadesStars = np.zeros(35)
firstline = False
convPoint = np.array([96.6*np.pi/180,5.8*np.pi/180],dtype = 'object')
avDistance = 0
avParalaxDist = 0

meanError = 0
ParalaxMeanError = 0

with open('hyades_data.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    i = 0
    for row in csv_reader:
        if firstline == False:
            firstline = True
            continue
        else:
            #names
            names[i] = str(row[0])
            #Degrees and hours to rad transformation
            RA =  (float(row[7])+float(row[8])/60+float(row[9])/3600)*(np.pi/12)
            DEC =  (float(row[10])+float(row[11])/60+(float(row[12])/3600))*(np.pi/180)
            
            #mu in rads
            ma = float(row[13])/206265*1e-3                    
            md = float(row[15])/206265*1e-3                    
            mu[i] = np.sqrt(ma*ma+md*md)                       
            #error of mu
            errorma = float(row[14])/206265*1e-3
            errormd = float(row[16])/206265*1e-3
            errormu[i] = np.sqrt((ma/mu[i])**2*errorma**2
                                +(md/mu[i])**2*errormd**2)
            
            #theta
            theta[i] = np.arccos(np.sin(DEC)*np.sin(convPoint[1])
            + np.cos(DEC)*np.cos(convPoint[1])*np.cos(convPoint[0]-RA))
            
            #TangVelocity in km/sec
            TangVel[i] = np.tan(theta[i])*float(row[3])
            #error of TangVelocity
            errorTangVel[i] = np.tan(theta[i])*float(row[4])
            
            #Distance in pc
            Distance[i] = TangVel[i]/(4.74*mu[i]*206265)      #mu back to ''
            #error of Distance
            errorDistance[i] = np.sqrt( (1/(4.74*mu[i]*206265))**2*errorTangVel[i]**2
                    +(-TangVel[i]/(4.74*(mu[i]*206265)**2))**2*errormu[i]**2)

            #Paralax Distance
            paralaxDist[i] = 1/(float(row[1])*1e-3)
            #error of Paralax Distance
            errorParalaxDist[i] = 1/((float(row[1])*1e-3)**2)*(float(row[2])*1e-3)
            #error of difference of the two methods
            diffError[i] = np.sqrt(errorParalaxDist[i]**2+errorDistance[i]**2)

            i += 1
    j = 0
    k = 0

    for i in range(0,35):
        if (Distance[i] > 56.34 and errorDistance[i] < 10) or (Distance[i] < 36.34 and errorDistance[i] < 10):
            continue
        else:
            avDistance += Distance[i]
            meanError += errorDistance[i]*errorDistance[i]
            j+=1

        if (paralaxDist[i] > 56.34 and errorParalaxDist[i] < 10) or (paralaxDist[i] < 36.34 and errorParalaxDist[i] < 10):
            continue
        else:
            avParalaxDist += paralaxDist[i]
            ParalaxMeanError += errorParalaxDist[i]*errorParalaxDist[i]
            k+=1


    avDistance = avDistance/j
    meanError = np.sqrt(meanError)/j
    avParalaxDist = avParalaxDist/k
    ParalaxMeanError = np.sqrt(ParalaxMeanError)/k
    print(avDistance, meanError)
    print(avParalaxDist, ParalaxMeanError)
    

# Need to run one time, can be commented out afterwards. 

with open('results.csv', mode='w') as csv_file:
    csv_writer = csv.writer(csv_file)
    i = 0
    for i in range(-1,35):
        if i == -1:
            csv_writer.writerow(['HIP','θ','ve','μ','rπ','rμ','rπ-rm'])
        else:
            csv_writer.writerow([int(names[i]),
                float("{:.3f}".format(theta[i]*180/np.pi)),
                str(float("{:.3f}".format(TangVel[i])))+'±'+str(float("{:.3f}".format(errorTangVel[i]))),
                str(float("{:.3f}".format(mu[i]*1e+3*206265)))+'±'+str(float("{:.3f}".format(errormu[i]*1e+3*206265))),
                str(float("{:.3f}".format(paralaxDist[i])))+'±'+str(float("{:.3f}".format(errorParalaxDist[i]))),
                str(float("{:.3f}".format(Distance[i])))+'±'+str(float("{:.3f}".format(errorDistance[i]))),
                str(float("{:.3f}".format(paralaxDist[i]-Distance[i])))+'±'+str(float("{:.3f}".format(diffError[i])))])
            i += 1



# This code block is for exporting the plot in a pgf format for
# further implementation on latex

# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     "pgf.texsystem": "xelatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': True,
# })

# For the distance-distance plot

fig = plt.figure(figsize=(8, 8))
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)


ax1.set_xlabel("Distance from Parallax Method (pc)")
ax1.set_ylabel("Distance from Convergent Point Method (pc)")


ax1.errorbar(paralaxDist,Distance,yerr=errorDistance,xerr=errorParalaxDist,fmt='k.')
ax1.plot(paralaxDist,Distance,'.',mfc='None')
ax1.fill_between(np.linspace(30,100,10000), 30, 65, facecolor='gray', hatch = '/')
ax1.fill_between(np.linspace(36.34,56.34,1000), 36.34, 56.34, facecolor='white')

ax1.set_xlim(30,100)
ax1.set_ylim(30,65)

ax2.errorbar(paralaxDist,Distance,yerr=errorDistance,xerr=errorParalaxDist,fmt='k.',mfc='white')
ax2.plot(paralaxDist,Distance,'.',mfc='white')
ax2.set_xlim(40,56.34)
ax2.set_ylim(36.34,52.5)

ax2.set_xlabel("Distance from Parallax Method (pc)")
ax2.set_ylabel("Distance from Convergent Point Method (pc)")

# For the histogram

# ax = fig.add_subplot(111)
# ax.set_xlabel("Error of Distance (pc)")
# ax.set_ylabel("Frequency")
# n,bins,patches = ax.hist([errorDistance,errorParalaxDist],bins=7,facecolor='none',
#     label=["Convergent Point","Parallax"])
# plt.setp(patches[0], hatch="/",edgecolor='orangered')
# plt.setp(patches[1], hatch="\\",edgecolor='blue')
# plt.legend(loc='best')
#ax.hist(,bins=9,facecolor='none',hatch='\\',edgecolor = 'blue')


plt.show()


#plt.savefig('plot.pgf',dpi=800,bbox_inches='tight')